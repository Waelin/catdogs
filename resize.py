import matplotlib.image as mpimg
import numpy as np
import scipy.misc as misc
import sys
import os

# Usage of this script: python resize.py data\train\ data\grayscale\train

def convertImageToGrayscaleSquare(pathImageIn, pathImageOut, size = 500):
    image = mpimg.imread(pathImageIn)
    gray = np.dot(image[...,:3], [0.299, 0.587, 0.114])
    array = misc.imresize(gray, (size, size))
    misc.imsave(pathImageOut, array)


def convertFolderToGrayscale(pathFolderIn, pathFolderOut, size = 500, limit = -1):
    print('Converting all images from to grayscale (size=%d)' % size)
    print('Input folder:  %s' % pathFolderIn)
    print('Output folder: %s' % pathFolderOut)

    if not os.path.exists(pathFolderOut):
        print('Created output folder: %s' % pathFolderOut)
        os.makedirs(pathFolderOut)

    filesAmount = len([name for name in os.listdir(pathFolderIn) if os.path.isfile(os.path.join(pathFolderIn, name))])
    processedFiles = 0

    for filename in os.listdir(pathFolderIn):
        processedFiles += 1
        if filename.endswith(".jpg"): 
            print('Processing file: \'%s\' ...' % filename, end="")
            convertImageToGrayscaleSquare(
                os.path.join(pathFolderIn, filename),
                os.path.join(pathFolderOut, filename),
                size)    
            print(' done! (%d of %d files processed)' % (processedFiles, filesAmount))
        else:
            print('File %s does not look like an image' % filename)

        if limit > 0 & processedFiles > limit:
            break

limit = -1
if len(sys.argv) >= 5:
    limit = int(sys.argv[4])

convertFolderToGrayscale(sys.argv[1], sys.argv[2], int(sys.argv[3]), limit)
